import { useState } from 'react'
import './App.css'
import { RouterProvider } from "react-router-dom"
import router from './Routing/route'
import Navbar from './views/Components/Navbar'
import "./App.css"
import "animate.css/animate.min.css";


function App() {

  return (
    <>

      <RouterProvider router={router} />

    </>
  )
}

export default App
