import { createBrowserRouter } from "react-router-dom"
import Home from "../views/pages/Home"
import About from "../views/pages/About"
import Error from "../views/pages/Error"
import PrivateRoute from "../views/pages/privateRoute/PrivateRoute"
import Login from "../views/pages/Login"
import PublicRoute from "../views/pages/publicRoute/PublicRoute"
import Form from "../views/pages/Form"
import Validate from "../views/pages/formik/Formik"
import Carousel from "../views/pages/carousel/Carousel"
import Scroll from "../scroll/Scroll"
const router = createBrowserRouter([
    {
        path: "/",
        element: <Home />,
        // errorElement: <Error />,
        children: [
            {
                path: "/login",
                element: <PublicRoute component={Login} />
            },
            {
                path: "/about",
                element: <PrivateRoute component={About} />
            },
            {
                path: "/table",
                element: <Form />
            },
            {
                path: "/formik",
                element: <Validate />
            },
            {
                path: "/carousel",
                element: <Carousel />
            },
            {
                path: "/scroll",
                element: <Scroll />
            }
        ]
    },
])
export default router