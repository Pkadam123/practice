import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from "react-toastify"
const initialState = {
    crud: [],
    loader: false,
    filter: [],
    user: {}
}
export const crudSlice = createSlice({
    name: "crud",
    initialState,
    reducers: {
        filtered: (state, action) => {
            state.filter = state.crud.filter((e) => {
                return e.name.toLowerCase().includes(action.payload.toLowerCase())
            })
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getUser.fulfilled, (state, action) => {
            state.crud = action.payload
            state.loader = false
        }).addCase(addUser.fulfilled, (state, action) => {
            state.crud.push(action.payload)
            state.loader = false
        }).addCase(updateUser.fulfilled, (state, action) => {
            let obj = state.crud.find((e) => e.id == action.payload.id)
            obj.name = action.payload.name,
                obj.email = action.payload.email,
                obj.phone = action.payload.phone
            state.loader = false
        }).addCase(deleteUser.fulfilled, (state, action) => {
            state.crud = state.crud.filter((e) => e.id !== action.payload)
            state.loader = false
        }).addCase(logged.fulfilled, (state, action) => {
            state.user = action.payload
        })
            .addCase(addUser.pending, (state, action) => {
                state.loader = true
            }).addCase(updateUser.pending, (state, action) => {
                state.loader = true
            }).addCase(getUser.pending, (state, action) => {
                state.loader = true
            }).addCase(deleteUser.pending, (state, action) => {
                state.loader = true
            })
    }
})
export const logged = createAsyncThunk("user/login",
    async (thunkAPI) => {
        let data = await axios.post("https://dummyjson.com/auth/login", thunkAPI)
        return data.data
    }
)

export const getUser = createAsyncThunk(
    "user/get",
    async (thunkAPI) => {
        let data = await axios.get(import.meta.env.VITE_ENDPOINTS)
        return data.data

    }
)
export const addUser = createAsyncThunk(
    "user/add",
    async (thunkAPI) => {
        try {
            let data = await axios.post(import.meta.env.VITE_ENDPOINTS, thunkAPI)
            toast.success("added successfully", {
                position: "top-center"
            })
            return data.data
        } catch (err) {
            toast.error("failed to add", {
                position: "top-center"
            })
        }
    }
)
export const updateUser = createAsyncThunk(
    "user/update",
    async (thunkAPI) => {
        try {
            let data = await axios.put(`${import.meta.env.VITE_ENDPOINTS}/${thunkAPI.id}`, thunkAPI)
            toast.success("updated successfully!!", { position: "top-center" })
            return thunkAPI
        } catch (err) {
            toast.error("failed to update", {
                position: "top-center"
            })
        }
    }
)
export const deleteUser = createAsyncThunk(
    "user/delete",
    async (thunkAPI) => {
        try {
            let data = await axios.delete(`${import.meta.env.VITE_ENDPOINTS}/${thunkAPI}`)
            toast.success("deleted successfully!!", { position: "top-center" })
            return thunkAPI
        } catch (err) {
            toast.error("failed to delete", {
                position: "top-center"
            })
        }
    }
)
export const { filtered } = crudSlice.actions

export default crudSlice.reducer