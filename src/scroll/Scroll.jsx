import React from 'react'
import { AnimationOnScroll } from 'react-animation-on-scroll';

const Scroll = () => {
    return (
        <div className='container'>
            <div className='row gy-5'>
                <div className='col-8'>
                    <div className='inner'></div>
                </div>
                <div className='col-4'> <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <div className='inner'></div>
                </AnimationOnScroll></div>
                <div className='col-4'> <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <div className='inner'></div>
                </AnimationOnScroll></div>
                <div className='col-4'><AnimationOnScroll animateIn="animate__flip">
                    <div className='inner'></div>
                </AnimationOnScroll></div>
                <div className='col-4'> <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <div className='inner'></div>
                </AnimationOnScroll></div>
                <div className='col-2'> <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <div className='inner'></div>
                </AnimationOnScroll></div>
                <div className='col-10'> <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <div className='inner'></div>
                </AnimationOnScroll></div>
            </div>
        </div>
    )
}

export default Scroll