import { configureStore } from "@reduxjs/toolkit";
import crud from "./features/crud/crudSlice"
const store = configureStore({
    reducer: crud
})
export default store