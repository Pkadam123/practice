import React from 'react'
import styles from "./button.module.css"

function Button(props) {

    return (
        <>
            <button
                type={props?.type}
                onClick={props?.onClick}
                className={`${styles.button} ${props?.primary && styles.primary} ${props?.danger && styles.danger}`}>
                {props?.label}
            </button>
        </>
    )
}

export default Button