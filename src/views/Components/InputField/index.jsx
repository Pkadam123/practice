import React from 'react'
import styles from "./input.module.css"

export const index = (props) => {
    console.log('props?.checked', props?.checked)

    return (
        <>
            {
                props?.required &&
                <span style={{ color: "red" }}>*</span>
            }
            {
                props?.label
            }
            <input
                required={props?.required}
                name={props?.name}
                className={styles.input}
                checked={props?.checked}
                type={props?.type}
                value={props?.value}
                placeholder={props?.placeholder}
                onChange={(e) => props?.onChange(e)}
            />

        </>
    )
}
export default index
