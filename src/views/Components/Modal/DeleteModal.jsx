import React from 'react'
import styles from "./modal.module.css"
import Button from '../Button'
import { deleteUser } from '../../../features/crud/crudSlice'
import { useDispatch } from 'react-redux'
function DeleteModal(props) {
    const dispatch = useDispatch()
    console.log('props', props?.selectedData?.id)
    function closeModal() {
        props?.setDeleteModal(false)
    }
    function del() {
        console.log("Del")
        dispatch(deleteUser(props?.selectedData?.id))
        props?.setDeleteModal(false)
    }
    return (
        <div className={styles.body}
            onClick={closeModal}>
            <div className={styles.deleteModal}
                onClick={(e) => e.stopPropagation()}
            >
                Are you sure you want to delete!!<br></br>
                <Button danger={true} label="Delete" onClick={del} />
                <Button primary={true} label="Cancel" onClick={closeModal} />
            </div>
        </div>
    )
}

export default DeleteModal