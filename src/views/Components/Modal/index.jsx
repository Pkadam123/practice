import React from 'react'
import styles from "./modal.module.css"
function Modal(props) {
    console.log('props', props)
    function closeModal() {
        props?.setModal(false)
    }
    return (
        <div className={styles.body}
            onClick={closeModal}>
            <div className={styles.modal}
                onClick={(e) => e.stopPropagation()}
            >
                {props?.children}
            </div>
        </div>
    )
}
export default Modal