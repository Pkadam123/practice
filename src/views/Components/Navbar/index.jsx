import React, { useEffect } from 'react'
import styles from "./navbar.module.css"
import { Link, NavLink } from 'react-router-dom'
import { useSelector } from 'react-redux'

function Navbar() {
    const { user } = useSelector((state) => state)
    console.log('user.name', user)

    return (
        <>
            <div className={styles.navbar}>
                <h1 style={{ color: "white" }}>{user?.firstName}</h1>
                <ul>
                    <NavLink to="/about"
                        className={({ isActive }) => (isActive ?
                            styles.nav_active : styles.navlink)}
                    ><li>About</li>
                    </NavLink>
                    <NavLink to="/login"
                        className={({ isActive }) => (isActive ?
                            styles.nav_active : styles.navlink)}
                    ><li>Login</li>
                    </NavLink>
                    <NavLink to="/table"
                        className={({ isActive }) => (isActive ?
                            styles.nav_active : styles.navlink)}

                    ><li>Table</li>
                    </NavLink>
                    <NavLink to="/formik" className={({ isActive }) => (isActive ?
                        styles.nav_active : styles.navlink)}
                    ><li>Formik</li>
                    </NavLink>
                    <NavLink to="/carousel" className={({ isActive }) => (isActive ?
                        styles.nav_active : styles.navlink)}
                    ><li>carousel</li>
                    </NavLink>
                    <NavLink to="/scroll" className={({ isActive }) => (isActive ?
                        styles.nav_active : styles.navlink)}
                    ><li>scroll</li>
                    </NavLink>


                </ul>

            </div >
        </>
    )
}

export default Navbar