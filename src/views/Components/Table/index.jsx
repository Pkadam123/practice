import DataTable from "react-data-table-component";
import React from 'react'

function Table(props) {
    return (
        <DataTable
            {...props}
            pagination
            theme="dark"
        />
    )
}
export default Table