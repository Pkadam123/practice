import React, { useState } from 'react'
import Input from '../../Components/InputField'
import Modal from '../../Components/Modal'
import Button from '../../Components/Button'
import { useDispatch } from 'react-redux'
import { addUser, updateUser } from '../../../features/crud/crudSlice'
import styles from "./form.module.css"

function DataForm(props) {
    console.log('props?.selectedData', props?.selectedData?.gender)
    const [data, setData] = useState({
        ...props?.selectedData

    })
    console.log('data?.gender', data?.gender)
    const [errorr, setError] = useState(
        {
            name: {
                err: false,
                msg: ""
            },
            email: {
                err: false,
                msg: ""
            },
            phone: {
                err: false,
                msg: ""
            }
        }
    )
    function handleFileMsg(str) {
        console.log(str)
        // setError({
        //     ...errorr, [str]: {
        //         ...errorr.str,
        //         msg: "required",
        //         err: true
        //     }
        // })
        setError((prev) => ({
            ...prev,
            [str]: {
                ...prev.str,
                msg: "required",
                err: true
            }
        }))
    }
    console.log(data)
    console.log("arr", errorr)
    const dispatch = useDispatch()
    function handleSubmit(e) {
        e.preventDefault()
        switch (true) {
            case !data.name:
                handleFileMsg("name");
            case !data.email:
                handleFileMsg("email");
            case !data.phone:
                handleFileMsg("phone");
        }
        if (!data.name || !data.email || !data.phone) {
            return
        }

        if (Object.keys(props?.selectedData).length == 0) {
            dispatch(addUser(data))
            props?.setModal(false)
        }
        else {
            dispatch(updateUser(data))
            props?.setModal(false)
        }
    }
    function handleChange(e) {
        console.log('handle')
        setData({ ...data, [e.target.name]: e.target.value })
    }
    return (
        <>
            <Modal
                setModal={props?.setModal}
            >
                <form onSubmit={handleSubmit} style={{ display: "flex", flexDirection: "column", alignItems: "center", gap: "8px" }} className={styles.form}>
                    <div>  <Input type="text" value={data?.name} name="name" onChange={handleChange} required={true} label={"Name"} error={errorr}
                    />{errorr.name.err && <span style={{ color: "red" }}>{errorr.name.msg}</span>}<br></br>
                    </div>
                    <div> <Input type="text" value={data?.email} name="email" onChange={handleChange} label={"Email"} error={errorr} />{errorr.email.err && <span style={{ color: "red" }}>{errorr.email.msg}</span>}<br></br></div>
                    <div> <Input type="text" value={data?.phone} name="phone" onChange={handleChange} label={"Phone"} error={errorr} />{errorr.phone.err && <span style={{ color: "red" }}>{errorr.phone.msg}</span>}<br></br></div>
                    <div><Input type="radio" name="gender" label="Male" value="male" onChange={handleChange} checked={props?.selectedData?.gender == "male"} /></div>
                    <div>
                        <Input type="radio" name="gender" label="Female" value="female" onChange={handleChange} checked={props?.selectedData?.gender == "female"} />
                    </div>
                    <div> <Button primary={true} label={Object.keys(props?.selectedData).length == 0 ? "Add" : "Save"} type="submit" error={errorr} /></div>
                    <div>
                    </div>
                    <div> <Button danger={true} label="Cancel" type="button" onClick={() => {
                        props?.setModal(false)
                    }} /></div>
                </form>
            </Modal>
        </>
    )
}

export default DataForm