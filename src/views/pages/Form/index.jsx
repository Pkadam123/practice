import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getUser } from '../../../features/crud/crudSlice'
import Table from '../../Components/Table'
import Button from '../../Components/Button'
import Modal from '../../Components/Modal'
import DataForm from './dataForm'
import DeleteModal from '../../Components/Modal/DeleteModal'
import Loader from '../../Components/Loader/Loader'
import Input from '../../Components/InputField'
import { filtered } from '../../../features/crud/crudSlice'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

function Form() {
    const [modal, setModal] = useState(false)
    const [deleteModal, setDeleteModal] = useState(false)
    const [selectedData, setSelectedData] = useState({})
    const dispatch = useDispatch()
    const { crud: data, loader, filter } = useSelector((state) => state)
    function handleEdit(data) {
        setSelectedData(data)
        setModal(true)
    }
    function handleDelete(data) {
        setSelectedData(data)
        setDeleteModal(true)
    }
    function handleadd() {
        setSelectedData({})
        setModal(true)
    }
    function handlechange(e) {
        let timeout = setTimeout(() => {
            dispatch(filtered(e.target.value))
        }, 1000);
        return () => {
            clearTimeout(timeout)
        }
    }
    useEffect(() => {
        dispatch(getUser())
    }, [])
    const columns = [
        {
            name: 'Name',
            selector: row => row.name,
        },
        {
            name: 'Email',
            selector: row => row.email,
        },
        {
            name: 'Phone',
            selector: row => row.phone,
        },
        {
            name: 'Gender',
            selector: row => row.gender,
        },
        {
            name: "Actions",
            cell: row => (
                <div style={{ display: "flex", gap: "10px" }}>
                    <Button
                        label="Edit"
                        primary={true}
                        onClick={() => handleEdit(row)}
                    />
                    <Button
                        label="Delete"
                        danger={true}
                        onClick={() => handleDelete(row)}
                    />
                </div>)
        }
    ];
    return (

        <>
            <SkeletonTheme baseColor="grey" highlightColor="#444">
                <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <Button primary={true} label="Add" onClick={handleadd} />
                    <Input type="text" onChange={handlechange} placeholder="search here" />
                </div>
                {
                    loader ? <Skeleton count={11} height={50} /> : <Table
                        data={filter.length == 0 ? data : filter}
                        columns={columns}
                    />
                }
                {
                    modal &&
                    <DataForm
                        setModal={setModal}
                        modal={modal}
                        selectedData={selectedData} />
                }
                {
                    deleteModal &&
                    <DeleteModal
                        setDeleteModal={setDeleteModal}
                        setSelectedData={setSelectedData}
                    />
                }
            </SkeletonTheme>
        </>




    )
}
export default Form