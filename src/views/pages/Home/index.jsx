import React from 'react'
import { Outlet } from 'react-router-dom'
import Navbar from '../../Components/Navbar'
import styles from "./home.module.css"

function Home() {
    return (
        <div>
            <Navbar />
            <div className={styles.content}>
                <Outlet />
            </div>
        </div>
    )
}

export default Home