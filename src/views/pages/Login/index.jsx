import React, { useState } from 'react'
import Input from '../../Components/InputField'
import { useDispatch } from 'react-redux'
import { logged } from '../../../features/crud/crudSlice'
import Button from '../../Components/Button'
import { useSelector } from 'react-redux/es/hooks/useSelector'
import { useNavigate } from 'react-router-dom'

function Login() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const { user } = useSelector((state) => state)
    const [data, setData] = useState({})
    function handlesubmit(e) {
        e.preventDefault()
        dispatch(logged(data))
        localStorage.setItem("auth", JSON.stringify(user))
        if (user?.token) {
            navigate("/about")
        }
        setData({
            username: "",
            password: ""
        })
    }
    function handlechange(e) {
        setData({
            ...data,
            [e.target.name]: e.target.value
        })
    }
    return (
        <div>
            <form style={{ border: "1px solid", display: "flex", flexDirection: "column", width: "30%", margin: "90px auto", gap: "10px", borderRadius: "10px", padding: "70px" }} onSubmit={handlesubmit}>
                <Input type="text" label="Username" onChange={handlechange} name="username" value={data?.username} />
                <Input type="password" label="Password" name="password" value={data?.password} onChange={handlechange} />
                <div style={{ textAlign: "center" }}><Button type="submit" primary={true} label="Login" />
                </div>
            </form>
        </div>
    )
}

export default Login