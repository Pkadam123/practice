import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper/modules';
import axios from 'axios';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import React, { useEffect, useState } from 'react';
const Carousel = () => {
    const [img, setImg] = useState([])
    useEffect(() => {
        let call = async () => {
            let data = await axios.get("https://api.slingacademy.com/v1/sample-data/photos")
            setImg(data.data.photos)
        }
        call()
    }, [])
    return (
        <Swiper
            modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
            spaceBetween={0}
            slidesPerView={1}
            navigation
            pagination={{ clickable: true }}
            scrollbar={{ draggable: true }}
            onSwiper={(swiper) => console.log(swiper)}
            onSlideChange={() => console.log('slide change')}
            autoplay={{ delay: 3000 }}
            style={{ height: "500px" }}>
            {
                img?.map((e) => {
                    return <SwiperSlide>
                        <img src={e.url} alt='not found' style={{ width: "100%" }} />
                    </SwiperSlide>
                })
            }
        </Swiper>
    );
};
export default Carousel