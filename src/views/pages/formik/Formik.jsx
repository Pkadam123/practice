import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import styles from "./Formik.module.scss"

const Validate = () => {
    const SignupSchema = Yup.object().shape({
        firstName: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        lastName: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        email: Yup.string().email('Invalid email').required('Required'),
    });

    return (
        <div>

            <h1>Signup</h1>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    email: '',
                }}
                validationSchema={SignupSchema}
                validateOnBlur={false}
                onSubmit={values => {
                    // same shape as initial values
                    console.log(values);
                }}
            >
                {({ errors, touched }) => (
                    <Form className={styles.form}>

                        <Field name="firstName" className={styles.field} />
                        <div className={styles.error}>
                            {errors.firstName && touched.firstName ? (
                                <div>{errors.firstName}</div>
                            ) : null}
                        </div>
                        <Field name="lastName" className={styles.field} />
                        <div className={styles.error}>
                            {errors.lastName && touched.lastName ? (
                                <div>{errors.lastName}</div>
                            ) : null}
                        </div>
                        <Field name="email" type="email" className={styles.field} />
                        <div className={styles.error}>  {errors.email && touched.email ? <div>{errors.email}</div> : null}</div>
                        <div>
                            <button type="submit" className={styles.btn}>Submit</button>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default Validate