import React from 'react'
import getToken from '../../../Utils'
import { Navigate } from 'react-router-dom'
function PrivateRoute({ component: Component }) {
    return (
        getToken() ? <Component /> : <Navigate to="/login" />
    )
}
export default PrivateRoute