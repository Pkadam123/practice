import React from 'react'
import getToken from '../../../Utils'
import { Navigate } from 'react-router-dom'

function PublicRoute({ component: Component }) {
    return (
        !getToken() ? <Component /> : <Navigate to="/about" />
    )
}
export default PublicRoute